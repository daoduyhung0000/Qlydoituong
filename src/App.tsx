import { Box, styled } from "@mui/material";
import Grid2 from "@mui/material/Unstable_Grid2";
import DataRender from "./components/DataRender";
import Form from "./components/Form";

const CustomBox = styled(Box)`
    padding: 1rem;
    box-sizing: border-box;
  `
function App() {
  
  return (
    <>
    <CustomBox>
      <Grid2 spacing={2}>
        <Form/>
        <DataRender/>
      </Grid2>
    </CustomBox>
    </>
    
  )
}

export default App;
