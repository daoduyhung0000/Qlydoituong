import { Button, Card, CardActions, CardContent, Typography } from "@mui/material"
import Grid2 from "@mui/material/Unstable_Grid2"
import { useDispatch, useSelector } from "react-redux"
import styled from "styled-components"
import { DataList, remove } from "../features/listDataReducer"
import { RootState } from "../store"

const Container = styled(Grid2)`
    width:100%
`
const DataRender: React.FC = () => {
    const data = useSelector((state: RootState) => state.dataList.value)
    const changeColorState = useSelector((state: RootState) => state.changeColor)
    const dispatch = useDispatch()
    function deleteData(key: number){
        dispatch(remove(key))
    }
    const changeColor = (age: number) => {
        if(age <= 18) return `#a2b369`;
        if(age > 18 && age <= 60) return `#efd21e`;
        if(age > 60) return `#e54631`;
    }
    const renderListData = () => {
        return data.map((value: DataList, key: number) => {
            return (
                <Grid2 xs={4} key={key}>
                    <Card sx={{
                        backgroundColor: changeColorState.value ? changeColor(value?.age): `white`
                    }}>
                        <CardContent>
                            <Typography>{`Name: ${value?.firstName} ${value?.lastName}`}</Typography>
                            <Typography>{`Age: ${value?.age}`}</Typography>
                            <Typography>{`Address: ${value?.address}`}</Typography>
                            <CardActions>
                                <Button onClick={() => deleteData(key)}>DELETE</Button>
                            </CardActions>
                        </CardContent>
                    </Card>
                </Grid2>
            )
        })
    }
    return (
        <Grid2 xs={12}>
            <Container container spacing={4}>
                {renderListData()}
            </Container>
        </Grid2>
    )
}

export default DataRender