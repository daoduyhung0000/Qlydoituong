import styled from "styled-components";
import { Button, FormControlLabel, TextField } from "@mui/material"
import Grid2 from "@mui/material/Unstable_Grid2"
import { useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../store";
import { add, DataList, removeAll } from "../features/listDataReducer";
import  Checkbox  from "@mui/material/Checkbox";
import { change } from "../features/showColorReducer";

const CustomTextField = styled(TextField)`
        width: 100%;
    `
    const Block = styled.div`
        display: flex;
        justify-content: space-between;
    `
const Container = styled(Grid2)`
    width:100%
`
const Total = styled.span`
    margin-left: 1rem;
`
const ColorShow = styled.span`
    background-color: ${props => props.color};
    margin: 0 10px;
    padding: 5px;
    border-radius: 5px;

`
const Form: React.FC = () => {  
    const firstNameRef = useRef({value: ''})
    const lastNameRef = useRef({value:''})
    const ageRef = useRef({value: NaN})
    const addressRef = useRef({value: ''})

    const dispatch = useDispatch()
    const data = useSelector((state: RootState) => state.dataList)
    const changeColorState = useSelector((state: RootState) => state.changeColor)
    function clearData() {
        dispatch(removeAll())
    }

    return (
        <Grid2 xs={12}>
            <form onSubmit={
                (e) => {
                    e.preventDefault()
                    const payload: DataList = {
                        firstName: firstNameRef.current.value,
                        lastName: lastNameRef.current.value,
                        age: ageRef.current.value,
                        address: addressRef.current.value
                    }
                    dispatch(add(payload))
                }
            }>
                <Container container rowSpacing={2} columnSpacing={6}>
                    <Grid2 xs={6}>
                        <CustomTextField label="First name:" inputRef={firstNameRef}/>
                    </Grid2>
                    <Grid2 xs={6}>
                        <CustomTextField label="Last name:" inputRef={lastNameRef}/>
                    </Grid2>
                    <Grid2 xs={6}>
                        <CustomTextField type="number" label="Age:" inputRef={ageRef}/>
                    </Grid2>
                    <Grid2 xs={6}>
                        <CustomTextField label="Address:" inputRef={addressRef} />
                    </Grid2>
                    <Grid2 xs={12}>
                        <Block>
                        <div>
                        <Button type="submit" value="Save" variant="outlined">SAVE</Button>
                        <Total>{`Tổng số: ${data.value.length}`}</Total>
                        {changeColorState.value && (
                            <>
                            <ColorShow color={`#a2b369`}>Dưới 18</ColorShow>
                            <ColorShow color={`#efd21e`}>Trên 18 dưới 60 </ColorShow>
                            <ColorShow color={`#e54631`}>Trên 60</ColorShow>
                            </>
                        )}
                        </div>
                        
                        <Button variant="outlined" onClick={()=> clearData()}>Clear all data</Button>
                        </Block>
                        <FormControlLabel 
                            control={<Checkbox onClick={(event: any) => dispatch(change(event.target.checked))}/>}
                            label= "Change color"
                        />
                    </Grid2>
                </Container>
            </form>
        </Grid2>
    )
}

export default Form