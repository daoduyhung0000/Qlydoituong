import { createSlice } from "@reduxjs/toolkit";

export interface DataList {
    firstName: string,
    lastName: string,
    age: number,
    address: string
}
export interface listDataState {
    value: DataList[]
}
const initialState: listDataState = {
    value: []

}
export const listDataSlice = createSlice({
    name:'listData',
    initialState,
    reducers: {
        add: (state: listDataState, action: {payload: DataList}) => {
            const copy = [...state.value]
            state.value = copy.concat(action.payload)
        },
        removeAll: (state: listDataState) => {
            state.value = []
        },
        remove: (state: listDataState, action: {payload: number}) => {
            const copy = [...state.value]
            copy.splice(action.payload, 1)
            state.value = copy
        }
    }
})

export const {add , removeAll, remove} = listDataSlice.actions

export default listDataSlice.reducer