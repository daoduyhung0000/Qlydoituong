import { createSlice } from "@reduxjs/toolkit"

export interface ChangeColorState {
    value: boolean
}
const initialState: ChangeColorState = {
    value: false
}

export const ShowColorReducer = createSlice({
    name: 'showColor',
    initialState,
    reducers: {
        change: (state: ChangeColorState, action: {payload: boolean}) => {
            state.value = action.payload
        }
    }
})

export const {change} = ShowColorReducer.actions

export default ShowColorReducer.reducer