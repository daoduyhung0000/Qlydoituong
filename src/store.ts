import { configureStore } from '@reduxjs/toolkit'
import listDataReducer from './features/listDataReducer'
import showColorReducer from './features/showColorReducer'

export const store = configureStore({
  reducer: {
    dataList: listDataReducer,
    changeColor: showColorReducer
  },
})


export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch